#include <iostream>
#include <vector>
#include <string>
#include <sstream>

struct SuperData
{
    uint16_t value;
    uint32_t id;
    bool isRed;

    SuperData(uint16_t val, uint32_t ID, bool red) { value = val; id = ID; isRed = red; };

    std::string toString() { return "val is: " + std::to_string(value) + " id is: " + std::to_string(id) + " and red is: " + std::to_string(isRed); }
};


int main()
{
    //from ascii table, Hello is : 104 101 108 108 111 

    typedef char byte;

    byte h{ 104 };
    byte e{ 101 };
    byte l{ 108 };
    byte o{ 111 };

    std::vector<byte> byteStreamer{h,e,l,l,o};

    for (const auto& r : byteStreamer) {
        
        std::cout << r <<std::endl;
    }

    SuperData myData(22, 6895, true);

    std::cout << "before to byte the SupeData object is: "+ myData.toString() << std::endl;

    std::cout << "the size of the myData object is: " + std::to_string(sizeof(myData)) + " bytes" << std::endl;


    //casting the class to byte array
    byte* byteStream = reinterpret_cast<char*>(&myData);

    //std::stringstream byteStream((char*)&myData);

    uint16_t value;
    uint32_t id;
    bool red;

    memcpy(&value, byteStream, sizeof(uint16_t));

    std::cout << "the new value for value is " + std::to_string(value) << std::endl;

    const uint16_t padding_bytes = 2;

    byteStream += padding_bytes;

    byteStream += sizeof(uint16_t);

    memcpy(&id, byteStream, sizeof(uint32_t));

    std::cout << "the new value for ID is " + std::to_string(id) << std::endl;
    byteStream += sizeof(uint32_t);

    memcpy(&red, byteStream, sizeof(bool));

    std::cout << "the new value for red is " + std::to_string(red) << std::endl;

    //casting the first 2 byte to have the value variable
   

}